# Doing Survey Research (University of Edinburgh)
*This repository was previously hosted on GitHub*

During my time as senior tutor of Doing Survey Research (2015--2019), I put together ten separate exercises that introduced social science students to Stata. Over the years, simply by repetition and thanks to informal feedback during lab sessions, I have been modifying the exercises, changing the datasets, making the examples more clear and approachable. The result is a nine-lesson introduction to Stata that tries to be as friendly and easy as possible. The intended audience of this project is social science students that have no prior knowledge of Stata, programming, and the like. However, some basic statistical knowledge is supposed, for these exercises are a practical, hands-on complement to course lectures. 

# Structure of the repository
This repository contains all teaching materials (lab sessions) for the course Doing Survey Research. The booklet has been produced with LaTeX, and it uses a root file that calls in each lab session. These sessions are written in separate files for simplicity's sake. All sessions are in the [tex folder](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/tree/master/tex) of the repository. Images are also in their own directory, the [img folder](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/tree/master/img).

The main document is named [root](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/blob/master/root.tex), so is the [PDF version](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/blob/master/root.pdf). Since everything goes the modular way, the [cover page](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/blob/master/coverpage.tex) has its own separate file too.

You can compile the whole booklet using the root file, or you can use the tex files in the folder [individuals](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/tree/master/individuals) to compile each lab session separately without a coverpage.

There is also a "cheatsheet" that gathers some basic Stata commands used throughout the exercises. This too has its [own directory in the repository](https://gitlab.com/Yuji_Shimohira_Calvo/DSR/-/tree/master/Stata_cheatsheet).

# Packages needed

You will need a number of rather standard packages to compile the .tex files in this repository. If you do not wish to compile them, you can download the latest PDF version. The packages used are:

* babel
* inputenc
* fourier
* fancyhdr
* parskip
* hyperref
* graphicx
* float
* listings
* tikz
* textcomp

# Acknowledgements
The exercises I have put together here are inevitably influenced by years of reading textbooks and looking up materials on the Internet for my master's degrees and Ph.D. I took ideas and teaching methodologies from here and there, but never "copied-pasted" exercises (meaning that feedback is welcome!). Many exercises use data from the European Social Survey (ESS). I believe the use of ESS data is allowed for teaching purposes. However, please contact me if you find a potential legal conflict in these materials. Finally, I would like to thank Dr. Alex Janus for giving me the opportunity to develop these materials for his course at the University of Edinburgh.
